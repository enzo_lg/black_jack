/*
 * Projet Noël Serveur
 * Créé le 19/12/2023
 * GitHub Nicolas-g22
 * Fichier: main.cpp
*/

#include "serveurbj.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ServeurBJ w;
    w.show();
    return a.exec();
}
