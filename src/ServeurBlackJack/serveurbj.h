/*
 * Projet Noël Serveur
 * Créé le 19/12/2023
 * GitHub Nicolas-g22
 * Fichier: serveurbg.h
*/

#ifndef SERVEURBJ_H
#define SERVEURBJ_H

#include <QWidget>
#include <QGridLayout>
#include <QTcpSocket>
#include <QTcpServer>
#include <QDataStream>
#include <QBuffer>
#include <QDebug>
#include <QRandomGenerator>
#include <QDateTime>
#include <QThread>
#include <QPoint>
#include <QSqlQuery>
#include "carte.h"
#include "joueur.h"

QT_BEGIN_NAMESPACE
namespace Ui { class ServeurBJ; }
QT_END_NAMESPACE

class ServeurBJ : public QWidget
{
    Q_OBJECT

public:
    ServeurBJ(QWidget *parent = nullptr);
    ~ServeurBJ();
    int getIndexJoueur(QTcpSocket *joueur);
    void finDePartie();
    int calculeScore(Joueur *joueur);
    void joueurSuivant(Joueur *joueur);

public slots:
    void onQTcpSocket_connected();
    void onQTcpSocket_disconnected();
    void onQTcpSocket_errorOccured(QAbstractSocket::SocketError SocketError);
    void onQTcpSocket_readyRead();
    void onQTcpServer_newConnection();

private slots:
    void on_pushButtonLancerServeur_clicked();

    void on_pushButtonLancerPartie_clicked();

private:
    Ui::ServeurBJ *ui;
    QTcpServer socketEcoute;
    QList<Carte> paquet;
    QList<Joueur*> listeJoueur;
    int cptConnection=0;
    int tourJoueur=1;
    int cpt=0;


};
#endif // SERVEURBJ_H
