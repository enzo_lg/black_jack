/*
 * Projet Noël Serveur
 * Créé le 19/12/2023
 * GitHub Nicolas-g22
 * Fichier: serveurbg.cpp
*/

#include "serveurbj.h"
#include "ui_serveurbj.h"
#include <QSqlError>

ServeurBJ::ServeurBJ(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::ServeurBJ)
{
    ui->setupUi(this);
    ui->pushButtonLancerPartie->setEnabled(false);
    socketEcoute.setMaxPendingConnections(3);
    QStringList entetes = {"Joueur","mise", "Score", "Cartes"};
    ui->tableWidget->setHorizontalHeaderLabels(entetes);
    for (int row = 0; row < 3; ++row) {
        // Colonne Joueur
        QTableWidgetItem *itemJoueur = new QTableWidgetItem("Nom du Joueur");
        ui->tableWidget->setItem(row, 0, itemJoueur);

        // Colonne mise
        QTableWidgetItem *itemMise= new QTableWidgetItem("mise du joueur");
        ui->tableWidget->setItem(row, 1, itemMise);

        // Colonne Score
        QTableWidgetItem *itemScore = new QTableWidgetItem("Score du joueur");
        ui->tableWidget->setItem(row, 2, itemScore);

        // Colonne Cartes
        QTableWidgetItem *itemCartes = new QTableWidgetItem("Cartes du joueur");
        ui->tableWidget->setItem(row, 3, itemCartes);
    }

    // Affichage du tableau
    ui->tableWidget->show();

    //BDD
    QSqlDatabase bd;
    bd = QSqlDatabase::addDatabase("QMYSQL");
    bd.setHostName("172.18.58.7");
    bd.setDatabaseName("BlackJack");
    bd.setUserName("snir");
    bd.setPassword("snir");
    if(!bd.open()){
        qDebug()<<"Erreur de connexion à la bdd", bd.lastError();
    }
    else {
        qDebug()<<"Ouverture de la BDD";
    }

    //Connexion
    connect(&socketEcoute, &QTcpServer::newConnection, this, &ServeurBJ::onQTcpServer_newConnection);

    //création du paquet de carte
    for(int symboleint=0;symboleint<4;symboleint++){
        QString symbole;
        switch (symboleint) {
        case 0:
            symbole="pique";
            break;
        case 1:
            symbole="carreau";
            break;
        case 2:
            symbole="trefle";
            break;
        case 3:
            symbole="coeur";
            break;
        }
        for(int valeur=0;valeur<13;valeur++){
            Carte carte(symbole,valeur+1,false);
            paquet.append(carte);
        }
    }
    //mélange du paquet
    QRandomGenerator gen;
    gen.seed(QDateTime::currentMSecsSinceEpoch());
    int indiceCarte;
    for(int i=0;i<1000;i++){

        indiceCarte=gen.bounded(paquet.size());
        qDebug()<<indiceCarte;
        paquet.swapItemsAt(0,indiceCarte);
    }

}


void ServeurBJ::onQTcpSocket_connected()
{
    qDebug()<<"Connected";
}

void ServeurBJ::onQTcpSocket_disconnected()
{
    qDebug()<<"Disconnected";
    QTcpSocket *joueur=qobject_cast<QTcpSocket *>(sender());
    // Enregistrement finale des données dans la base de donnée
    // ...
    //deconnection suppression du joueur
    listeJoueur.removeAt(getIndexJoueur(joueur));

}

void ServeurBJ::onQTcpSocket_errorOccured(QAbstractSocket::SocketError SocketError)
{
    qDebug()<<"Error(erroreOccured): " + socketEcoute.errorString();
}

void ServeurBJ::onQTcpSocket_readyRead()
{
    qDebug()<<"ReadyRead";
    quint16 taille = 0;
    QChar commande;
    int score;
    QBuffer tampon;
    QString pseudo;
    QString mdp;
    QTcpSocket *joueur=qobject_cast<QTcpSocket *>(sender());
    QSqlQuery requetePrepare;
    int existance;
    QTableWidgetItem *itemJoueur;
    QTableWidgetItem *itemScore;
    QTableWidgetItem *itemCartes;
    QTableWidgetItem *itemMise;
    int row;
    int mise;
    int miseVerify;
    int cptCarte = 0;


    QDataStream out(&tampon);
    //indexClient = listeSocketsClient.indexOf(client);
    //QPoint positionClient = listePositions.at(indexClient);

    if(joueur->bytesAvailable() >= (quint16)sizeof(taille)){
        QDataStream in(joueur);
        in >> taille;
        if(joueur->bytesAvailable() >= (quint64)taille){
            in>>commande;
            switch (commande.toLatin1()){
            case 'N':
                // Pseudo du joueur
                // ...
                in>>pseudo>>mdp;
                qDebug()<< "un truc "<<pseudo << " " << mdp;
                requetePrepare.prepare("SELECT id FROM joueur WHERE pseudo = :pseudo AND mdp = :mdp;");
                requetePrepare.bindValue(":pseudo", pseudo);
                requetePrepare.bindValue(":mdp", mdp);

                if(!requetePrepare.exec()){
                    qDebug()<<"Request Issue"<<requetePrepare.lastError();
                }
                existance = requetePrepare.size();

                if(existance != 1){
                    requetePrepare.prepare("INSERT INTO joueur(pseudo, mdp, cagnotte) VALUES ( :pseudo , :mdp , 1000)");
                    requetePrepare.bindValue(":pseudo", pseudo);
                    requetePrepare.bindValue(":mdp", mdp);
                    if(!requetePrepare.exec()){
                        qDebug()<<"Request Issue"<<requetePrepare.lastError();
                    }
                }
                listeJoueur[getIndexJoueur(joueur)]->setNom(pseudo);
                ui->textEdit->append("le joueur "+pseudo+" s'est connecté");


                break;

            case 'M':
                // Miser
                // Modif des données puis renvoie les donnée
                // ...
                in>>mise;
                commande='M';
                if(mise >= 100 && mise<=listeJoueur[getIndexJoueur(joueur)]->getArgent()){
                    listeJoueur[getIndexJoueur(joueur)]->setMise(mise);
                    listeJoueur[getIndexJoueur(joueur)]->setArgent(-mise);
                    miseVerify = 0;
                    ui->textEdit->append("le joueur "+listeJoueur[getIndexJoueur(joueur)]->getNom()+" a miser "+QString::number(mise));

                }
                else{
                    // Trame
                    miseVerify = 1;

                }
                // Trame
                tampon.open(QIODevice::WriteOnly);
                // construction de la trame à envoyer au client
                out<<taille<<commande<<miseVerify<<mise<<listeJoueur[getIndexJoueur(joueur)]->getArgent();
                // calcul de la taille de la trame
                taille=(static_cast<quint16>(tampon.size()))-sizeof(taille);
                // placement sur la premiere position du flux pour pouvoir modifier la taille
                tampon.seek(0);
                //modification de la trame avec la taille reel de la trame
                out<<taille;
                // envoi du QByteArray du tampon via la socket
                joueur->write(tampon.buffer());

                break;

            case 'T':
                if(paquet.size()!=0){
                    commande='T';
                    // Bouton piocher appuyé
                    listeJoueur[getIndexJoueur(joueur)]->ajouterCarte(paquet.at(0));
                    ui->textEdit->append("le joueur "+listeJoueur[getIndexJoueur(joueur)]->getNom()+" a tirer la carte "+QString::number(paquet.at(0).getValeur())+" "+paquet.at(0).getSymbole());
                    paquet.removeAt(0);
                    cptCarte=listeJoueur[getIndexJoueur(joueur)]->getListCarte().size();
                    // generation de la liste des vols

                    tampon.open(QIODevice::WriteOnly);
                    // association du tampon au flux de sortie


                    listeJoueur[getIndexJoueur(joueur)]->setScore(listeJoueur[getIndexJoueur(joueur)]->getListCarte().last().getValeur());
                    // construction de la trame à envoyer au client
                    score=calculeScore(listeJoueur[getIndexJoueur(joueur)]);
                    out<<taille<<commande<<listeJoueur[getIndexJoueur(joueur)]->getListCarte()<<listeJoueur[getIndexJoueur(joueur)]->getScore()<<cptCarte;
                    // calcul de la taille de la trame
                    taille=(static_cast<quint16>(tampon.size()))-sizeof(taille);
                    // placement sur la premiere position du flux pour pouvoir modifier la taille
                    tampon.seek(0);
                    //modification de la trame avec la taille reel de la trame
                    out<<taille;
                    // envoi du QByteArray du tampon via la socket
                    joueur->write(tampon.buffer());

                    if(score==21){
                        listeJoueur[getIndexJoueur(joueur)]->setCoucher(true);
                        joueurSuivant(listeJoueur[getIndexJoueur(joueur)]);
                    }
                    if(score>21){
                        listeJoueur[getIndexJoueur(joueur)]->setCoucher(true);
                        joueurSuivant(listeJoueur[getIndexJoueur(joueur)]);
                    }
                }
                else{
                    finDePartie();
                }

                break;

            case 'R':
                // Bouton rester appuyé
                // Passe au tour du prochain
                // ...

                ui->textEdit->append("le joueur "+listeJoueur[getIndexJoueur(joueur)]->getNom()+" se couche");
                joueurSuivant(listeJoueur[getIndexJoueur(joueur)]);
                break;

            case 'P':
                // Bouton prêt appuyé
                // Mise à jour de l'état du joueur
                listeJoueur[getIndexJoueur(joueur)]->setPret(true);
                ui->textEdit->append("le joueur "+listeJoueur[getIndexJoueur(joueur)]->getNom()+" est prêt");
                break;
            }
        }
    }
    row = getIndexJoueur(joueur);



    // Colonne Joueur
    itemJoueur = new QTableWidgetItem(listeJoueur[getIndexJoueur(joueur)]->getNom());
    ui->tableWidget->setItem(row, 0, itemJoueur);

    //colonne mise
    itemMise = new QTableWidgetItem(QString::number(listeJoueur[getIndexJoueur(joueur)]->getMise()));
    ui->tableWidget->setItem(row, 1, itemMise);

    // Colonne Score
    itemScore = new QTableWidgetItem(QString::number(listeJoueur[getIndexJoueur(joueur)]->getScore()));
    ui->tableWidget->setItem(row, 2, itemScore);

    // Colonne Cartes
    itemCartes = new QTableWidgetItem(QString::number(listeJoueur[getIndexJoueur(joueur)]->getListCarte().size()));
    ui->tableWidget->setItem(row, 3, itemCartes);
    cpt+=1;



    bool pret=true;
    for(int idJoueur=0;idJoueur<listeJoueur.size();idJoueur++){
        if(listeJoueur[idJoueur]->getPret()==false){
            pret=false;
        }
    }
    if(pret==true){
        ui->pushButtonLancerPartie->setEnabled(true);
    }
}


void ServeurBJ::onQTcpServer_newConnection()
{
    if(cptConnection<3){
        qDebug()<<"New Connection";
        // Récup de la socket de communication joueur
        QTcpSocket *sockJoueur=socketEcoute.nextPendingConnection();

        // Connexion signal/slots
        connect(sockJoueur, &QTcpSocket::connected, this , &ServeurBJ::onQTcpSocket_connected);
        connect(sockJoueur, &QTcpSocket::disconnected, this , &ServeurBJ::onQTcpSocket_disconnected);
        connect(sockJoueur, &QTcpSocket::errorOccurred, this , &ServeurBJ::onQTcpSocket_errorOccured);
        connect(sockJoueur, &QTcpSocket::readyRead, this , &ServeurBJ::onQTcpSocket_readyRead);

        Joueur *joueur=new Joueur();
        joueur->setSockJoueur(sockJoueur);
        listeJoueur.append(joueur);

        quint16 taille = 0;
        QChar commande='C';
        QBuffer tampon;
        QDataStream out(&tampon);
        tampon.open(QIODevice::WriteOnly);
        // association du tampon au flux de sortie
        // construction de la trame à envoyer au client
        out<<taille<<commande<<joueur->getArgent();
        // calcul de la taille de la trame
        taille=(static_cast<quint16>(tampon.size()))-sizeof(taille);
        // placement sur la premiere position du flux pour pouvoir modifier la taille
        tampon.seek(0);
        //modification de la trame avec la taille reel de la trame
        out<<taille;
        // envoi du QByteArray du tampon via la socket
        sockJoueur->write(tampon.buffer());

        cptConnection++;
    }
    else{
        QTcpSocket *sockJoueur=socketEcoute.nextPendingConnection();
        sockJoueur->disconnectFromHost();
    }
}

void ServeurBJ::on_pushButtonLancerServeur_clicked()
{
    if(ui->pushButtonLancerServeur->text()=="Lancer Serveur"){
        // Lancement du serveur
        if(socketEcoute.listen(QHostAddress::Any,ui->spinBoxPort->value())){
            qDebug() << "Server started.";
            ui->pushButtonLancerServeur->setText("Fermer Serveur");
            ui->textEdit->append("serveur lancer");

        }
        // Erreur de lancement
        else{
            qDebug() << "Error(PushButtonLancer): " + socketEcoute.errorString();
        }
    }
    // Fermeture du serveur et deconnection des clients
    else{
        socketEcoute.close();
        foreach(Joueur *joueur, listeJoueur){
            joueur->getSockJoueur()->disconnectFromHost();
        }
        for(int idJoueur = 0;idJoueur<listeJoueur.size();idJoueur++){
            listeJoueur.removeAt(idJoueur);
        }

        qDebug() << "Server Closed";
        ui->pushButtonLancerServeur->setText("Lancer Serveur");
        ui->textEdit->append("serveur fermer");
    }
}

int ServeurBJ::getIndexJoueur(QTcpSocket *joueur)
{
    int index=0;
    int index_sauvegarder=-1;
    foreach (Joueur *joueur_courant, listeJoueur) {
        if (joueur==joueur_courant->getSockJoueur())
        {
            index_sauvegarder=index;

        }
        index++;
    }
    return index_sauvegarder;
}

void ServeurBJ::finDePartie()
{
    qDebug()<<"finPartie";
    int totaleMise = 0;
    //int score1 = listeJoueur[0]->getScore(), score2 = listeJoueur[1]->getScore() ,score3 = listeJoueur[2]->getScore();
    foreach(Joueur *joueur, listeJoueur){
        totaleMise+=joueur->getMise();
        joueur->setMise(0);
    }
    quint16 taille = 0;
    QChar commande='D';
    QBuffer tampon;
    QDataStream out(&tampon);
    foreach(Joueur *joueurCourant, listeJoueur){
        qDebug()<<"ici";
        commande='D';
        if(joueurCourant->getScore()==21){
            qDebug()<<"victoir";
            commande='V';
            tampon.open(QIODevice::WriteOnly);
            // association du tampon au flux de sortie
            // construction de la trame à envoyer au client
            out<<taille<<commande<<joueurCourant->getNom();
            // calcul de la taille de la trame
            taille=(static_cast<quint16>(tampon.size()))-sizeof(taille);
            // placement sur la premiere position du flux pour pouvoir modifier la taille
            tampon.seek(0);
            //modification de la trame avec la taille reel de la trame
            out<<taille;
            // envoi du QByteArray du tampon via la socket
            joueurCourant->getSockJoueur()->write(tampon.buffer());
        }

        if(joueurCourant->getScore() > 21){
            qDebug()<<"defaite";
            tampon.open(QIODevice::WriteOnly);
            // association du tampon au flux de sortie
            // construction de la trame à envoyer au client
            out<<taille<<commande<<joueurCourant->getNom();
            // calcul de la taille de la trame
            taille=(static_cast<quint16>(tampon.size()))-sizeof(taille);
            // placement sur la premiere position du flux pour pouvoir modifier la taille
            tampon.seek(0);
            //modification de la trame avec la taille reel de la trame
            out<<taille;
            // envoi du QByteArray du tampon via la socket
            joueurCourant->getSockJoueur()->write(tampon.buffer());
            joueurCourant->setScore(0);
        }

    }
    if(listeJoueur.size()==3){
        int score1 = listeJoueur[0]->getScore(), score2 = listeJoueur[1]->getScore() ,score3 = listeJoueur[2]->getScore();
        qDebug()<<"multi";
        if(score1>score2 && score1>score3){
            commande='V';
            tampon.open(QIODevice::WriteOnly);
            // association du tampon au flux de sortie
            // construction de la trame à envoyer au client
            out<<taille<<commande<<listeJoueur[0]->getNom();
            // calcul de la taille de la trame
            taille=(static_cast<quint16>(tampon.size()))-sizeof(taille);
            // placement sur la premiere position du flux pour pouvoir modifier la taille
            tampon.seek(0);
            //modification de la trame avec la taille reel de la trame
            out<<taille;
            // envoi du QByteArray du tampon via la socket
            listeJoueur[0]->getSockJoueur()->write(tampon.buffer());

            commande='D';
            tampon.open(QIODevice::WriteOnly);
            // association du tampon au flux de sortie
            // construction de la trame à envoyer au client
            out<<taille<<commande<<listeJoueur[1]->getNom();
            // calcul de la taille de la trame
            taille=(static_cast<quint16>(tampon.size()))-sizeof(taille);
            // placement sur la premiere position du flux pour pouvoir modifier la taille
            tampon.seek(0);
            //modification de la trame avec la taille reel de la trame
            out<<taille;
            // envoi du QByteArray du tampon via la socket
            listeJoueur[1]->getSockJoueur()->write(tampon.buffer());

            tampon.open(QIODevice::WriteOnly);
            // association du tampon au flux de sortie
            // construction de la trame à envoyer au client
            out<<taille<<commande<<listeJoueur[2]->getNom();
            // calcul de la taille de la trame
            taille=(static_cast<quint16>(tampon.size()))-sizeof(taille);
            // placement sur la premiere position du flux pour pouvoir modifier la taille
            tampon.seek(0);
            //modification de la trame avec la taille reel de la trame
            out<<taille;
            // envoi du QByteArray du tampon via la socket
            listeJoueur[2]->getSockJoueur()->write(tampon.buffer());

            listeJoueur[0]->setArgent(totaleMise);
        }
        else{
            if(score2>score1 && score2>score3){
                commande='V';
                tampon.open(QIODevice::WriteOnly);
                // association du tampon au flux de sortie
                // construction de la trame à envoyer au client
                out<<taille<<commande<<listeJoueur[1]->getNom();
                // calcul de la taille de la trame
                taille=(static_cast<quint16>(tampon.size()))-sizeof(taille);
                // placement sur la premiere position du flux pour pouvoir modifier la taille
                tampon.seek(0);
                //modification de la trame avec la taille reel de la trame
                out<<taille;
                // envoi du QByteArray du tampon via la socket
                listeJoueur[2]->getSockJoueur()->write(tampon.buffer());

                commande='D';
                tampon.open(QIODevice::WriteOnly);
                // association du tampon au flux de sortie
                // construction de la trame à envoyer au client
                out<<taille<<commande<<listeJoueur[0]->getNom();
                // calcul de la taille de la trame
                taille=(static_cast<quint16>(tampon.size()))-sizeof(taille);
                // placement sur la premiere position du flux pour pouvoir modifier la taille
                tampon.seek(0);
                //modification de la trame avec la taille reel de la trame
                out<<taille;
                // envoi du QByteArray du tampon via la socket
                listeJoueur[0]->getSockJoueur()->write(tampon.buffer());

                tampon.open(QIODevice::WriteOnly);
                // association du tampon au flux de sortie
                // construction de la trame à envoyer au client
                out<<taille<<commande<<listeJoueur[2]->getNom();
                // calcul de la taille de la trame
                taille=(static_cast<quint16>(tampon.size()))-sizeof(taille);
                // placement sur la premiere position du flux pour pouvoir modifier la taille
                tampon.seek(0);
                //modification de la trame avec la taille reel de la trame
                out<<taille;
                // envoi du QByteArray du tampon via la socket
                listeJoueur[2]->getSockJoueur()->write(tampon.buffer());

                listeJoueur[1]->setArgent(totaleMise);
            }
            else{
                commande='V';
                tampon.open(QIODevice::WriteOnly);
                // association du tampon au flux de sortie
                // construction de la trame à envoyer au client
                out<<taille<<commande<<listeJoueur[2]->getNom();
                // calcul de la taille de la trame
                taille=(static_cast<quint16>(tampon.size()))-sizeof(taille);
                // placement sur la premiere position du flux pour pouvoir modifier la taille
                tampon.seek(0);
                //modification de la trame avec la taille reel de la trame
                out<<taille;
                // envoi du QByteArray du tampon via la socket
                listeJoueur[2]->getSockJoueur()->write(tampon.buffer());

                commande='D';
                tampon.open(QIODevice::WriteOnly);
                // association du tampon au flux de sortie
                // construction de la trame à envoyer au client
                out<<taille<<commande<<listeJoueur[1]->getNom();
                // calcul de la taille de la trame
                taille=(static_cast<quint16>(tampon.size()))-sizeof(taille);
                // placement sur la premiere position du flux pour pouvoir modifier la taille
                tampon.seek(0);
                //modification de la trame avec la taille reel de la trame
                out<<taille;
                // envoi du QByteArray du tampon via la socket
                listeJoueur[1]->getSockJoueur()->write(tampon.buffer());

                tampon.open(QIODevice::WriteOnly);
                // association du tampon au flux de sortie
                // construction de la trame à envoyer au client
                out<<taille<<commande<<listeJoueur[1]->getNom();
                // calcul de la taille de la trame
                taille=(static_cast<quint16>(tampon.size()))-sizeof(taille);
                // placement sur la premiere position du flux pour pouvoir modifier la taille
                tampon.seek(0);
                //modification de la trame avec la taille reel de la trame
                out<<taille;
                // envoi du QByteArray du tampon via la socket
                listeJoueur[1]->getSockJoueur()->write(tampon.buffer());

                listeJoueur[2]->setArgent(totaleMise);
            }
        }
    }

}

int ServeurBJ::calculeScore(Joueur *joueur)
{
    int score = 0;
    foreach(Carte carte, joueur->getListCarte()){
        score+=carte.getValeur();
    }
    return score;
}

void ServeurBJ::joueurSuivant(Joueur *joueur)
{
    cpt=0;
    qDebug()<<"joueurSuivant";

    if(tourJoueur==listeJoueur.size()){
        qDebug()<<"Joueur suivant if";
        finDePartie();
    }
    else{
        qDebug()<<"Joueur suivant else";
        tourJoueur+=1;
        quint16 taille = 0;
        QChar commande='L';
        QBuffer tampon;
        QDataStream out(&tampon);
        Joueur *joueurSuivant=listeJoueur[tourJoueur-1];
        tampon.open(QIODevice::WriteOnly);
        // association du tampon au flux de sortie
        // construction de la trame à envoyer au client
        out<<taille<<commande;
        // calcul de la taille de la trame
        taille=(static_cast<quint16>(tampon.size()))-sizeof(taille);
        // placement sur la premiere position du flux pour pouvoir modifier la taille
        tampon.seek(0);
        //modification de la trame avec la taille reel de la trame
        out<<taille;
        // envoi du QByteArray du tampon via la socket
        joueurSuivant->getSockJoueur()->write(tampon.buffer());
    }
    quint16 taille = 0;
    QChar commande='R';
    QBuffer tampon;
    QDataStream out(&tampon);
    tampon.open(QIODevice::WriteOnly);
    // association du tampon au flux de sortie
    // construction de la trame à envoyer au client
    out<<taille<<commande;
    // calcul de la taille de la trame
    taille=(static_cast<quint16>(tampon.size()))-sizeof(taille);
    // placement sur la premiere position du flux pour pouvoir modifier la taille
    tampon.seek(0);
    //modification de la trame avec la taille reel de la trame
    out<<taille;
    // envoi du QByteArray du tampon via la socket
    joueur->getSockJoueur()->write(tampon.buffer());

}


ServeurBJ::~ServeurBJ()
{
    delete ui;
    for(int joueur=0; joueur<listeJoueur.size();joueur++){
        delete listeJoueur[joueur];
    }
}





void ServeurBJ::on_pushButtonLancerPartie_clicked()
{
    quint16 taille = 0;
    QChar commande='L';
    QBuffer tampon;
    QDataStream out(&tampon);
    Joueur *joueur=listeJoueur[0];
    tampon.open(QIODevice::WriteOnly);
    // association du tampon au flux de sortie
    // construction de la trame à envoyer au client
    out<<taille<<commande;
    // calcul de la taille de la trame
    taille=(static_cast<quint16>(tampon.size()))-sizeof(taille);
    // placement sur la premiere position du flux pour pouvoir modifier la taille
    tampon.seek(0);
    //modification de la trame avec la taille reel de la trame
    out<<taille;
    // envoi du QByteArray du tampon via la socket
    joueur->getSockJoueur()->write(tampon.buffer());
    ui->textEdit->append("partie lancer");
    ui->pushButtonLancerPartie->setEnabled(false);
}


