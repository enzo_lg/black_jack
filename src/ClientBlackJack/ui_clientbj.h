/********************************************************************************
** Form generated from reading UI file 'clientbj.ui'
**
** Created by: Qt User Interface Compiler version 6.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CLIENTBJ_H
#define UI_CLIENTBJ_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ClientBJ
{
public:
    QGraphicsView *maVue;
    QPushButton *pushButtonPiocher;
    QPushButton *pushButtonRegles;
    QPushButton *pushButtonRester;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayoutMontantMise;
    QLabel *labelMontantMise;
    QLabel *labelMontantMiseNumerique;
    QPushButton *pushButtonConnexion;
    QPushButton *pushButtonQuitter;
    QLineEdit *lineEditPseudo;
    QWidget *layoutWidget1;
    QVBoxLayout *verticalLayout;
    QGraphicsView *maMain;
    QHBoxLayout *horizontalLayout;
    QLabel *labelMise;
    QLineEdit *lineEditMise;
    QWidget *layoutWidget2;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *lineEditAdresse;
    QSpinBox *spinBoxPort;
    QWidget *layoutWidget3;
    QVBoxLayout *verticalLayoutCarte;
    QLabel *labelCarte;
    QLabel *labelCarteNumerique;
    QPushButton *pushButtonPret;
    QPushButton *pushButtonMiser;
    QWidget *layoutWidget_2;
    QVBoxLayout *verticalLayoutCagnotte;
    QLabel *labelCagnotte;
    QLabel *labelMontantCagnotte;
    QLineEdit *lineEditMdp;

    void setupUi(QWidget *ClientBJ)
    {
        if (ClientBJ->objectName().isEmpty())
            ClientBJ->setObjectName("ClientBJ");
        ClientBJ->resize(763, 600);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(ClientBJ->sizePolicy().hasHeightForWidth());
        ClientBJ->setSizePolicy(sizePolicy);
        ClientBJ->setMinimumSize(QSize(763, 600));
        ClientBJ->setMaximumSize(QSize(763, 600));
        maVue = new QGraphicsView(ClientBJ);
        maVue->setObjectName("maVue");
        maVue->setGeometry(QRect(10, 10, 541, 381));
        maVue->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        maVue->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        maVue->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        pushButtonPiocher = new QPushButton(ClientBJ);
        pushButtonPiocher->setObjectName("pushButtonPiocher");
        pushButtonPiocher->setGeometry(QRect(570, 450, 181, 41));
        QFont font;
        font.setPointSize(16);
        pushButtonPiocher->setFont(font);
        pushButtonRegles = new QPushButton(ClientBJ);
        pushButtonRegles->setObjectName("pushButtonRegles");
        pushButtonRegles->setGeometry(QRect(570, 10, 181, 31));
        pushButtonRester = new QPushButton(ClientBJ);
        pushButtonRester->setObjectName("pushButtonRester");
        pushButtonRester->setGeometry(QRect(570, 500, 181, 41));
        pushButtonRester->setFont(font);
        layoutWidget = new QWidget(ClientBJ);
        layoutWidget->setObjectName("layoutWidget");
        layoutWidget->setGeometry(QRect(450, 410, 111, 81));
        verticalLayoutMontantMise = new QVBoxLayout(layoutWidget);
        verticalLayoutMontantMise->setObjectName("verticalLayoutMontantMise");
        verticalLayoutMontantMise->setContentsMargins(0, 0, 0, 0);
        labelMontantMise = new QLabel(layoutWidget);
        labelMontantMise->setObjectName("labelMontantMise");
        QFont font1;
        font1.setPointSize(12);
        labelMontantMise->setFont(font1);
        labelMontantMise->setAlignment(Qt::AlignCenter);

        verticalLayoutMontantMise->addWidget(labelMontantMise);

        labelMontantMiseNumerique = new QLabel(layoutWidget);
        labelMontantMiseNumerique->setObjectName("labelMontantMiseNumerique");
        QFont font2;
        font2.setPointSize(30);
        labelMontantMiseNumerique->setFont(font2);
        labelMontantMiseNumerique->setAlignment(Qt::AlignCenter);

        verticalLayoutMontantMise->addWidget(labelMontantMiseNumerique);

        pushButtonConnexion = new QPushButton(ClientBJ);
        pushButtonConnexion->setObjectName("pushButtonConnexion");
        pushButtonConnexion->setGeometry(QRect(20, 520, 181, 31));
        pushButtonConnexion->setFont(font1);
        pushButtonQuitter = new QPushButton(ClientBJ);
        pushButtonQuitter->setObjectName("pushButtonQuitter");
        pushButtonQuitter->setGeometry(QRect(20, 560, 181, 31));
        pushButtonQuitter->setFont(font1);
        lineEditPseudo = new QLineEdit(ClientBJ);
        lineEditPseudo->setObjectName("lineEditPseudo");
        lineEditPseudo->setGeometry(QRect(20, 440, 181, 31));
        lineEditPseudo->setFont(font1);
        lineEditPseudo->setMaxLength(15);
        lineEditPseudo->setEchoMode(QLineEdit::Normal);
        layoutWidget1 = new QWidget(ClientBJ);
        layoutWidget1->setObjectName("layoutWidget1");
        layoutWidget1->setGeometry(QRect(570, 50, 181, 341));
        verticalLayout = new QVBoxLayout(layoutWidget1);
        verticalLayout->setObjectName("verticalLayout");
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        maMain = new QGraphicsView(layoutWidget1);
        maMain->setObjectName("maMain");
        maMain->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        maMain->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        maMain->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        verticalLayout->addWidget(maMain);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName("horizontalLayout");
        labelMise = new QLabel(layoutWidget1);
        labelMise->setObjectName("labelMise");
        labelMise->setFont(font1);

        horizontalLayout->addWidget(labelMise);

        lineEditMise = new QLineEdit(layoutWidget1);
        lineEditMise->setObjectName("lineEditMise");

        horizontalLayout->addWidget(lineEditMise);


        verticalLayout->addLayout(horizontalLayout);

        layoutWidget2 = new QWidget(ClientBJ);
        layoutWidget2->setObjectName("layoutWidget2");
        layoutWidget2->setGeometry(QRect(20, 398, 181, 31));
        horizontalLayout_2 = new QHBoxLayout(layoutWidget2);
        horizontalLayout_2->setObjectName("horizontalLayout_2");
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        lineEditAdresse = new QLineEdit(layoutWidget2);
        lineEditAdresse->setObjectName("lineEditAdresse");
        lineEditAdresse->setFont(font1);

        horizontalLayout_2->addWidget(lineEditAdresse);

        spinBoxPort = new QSpinBox(layoutWidget2);
        spinBoxPort->setObjectName("spinBoxPort");
        spinBoxPort->setFont(font1);
        spinBoxPort->setMinimum(1024);
        spinBoxPort->setMaximum(9999);
        spinBoxPort->setValue(5555);

        horizontalLayout_2->addWidget(spinBoxPort);

        layoutWidget3 = new QWidget(ClientBJ);
        layoutWidget3->setObjectName("layoutWidget3");
        layoutWidget3->setGeometry(QRect(210, 410, 131, 81));
        verticalLayoutCarte = new QVBoxLayout(layoutWidget3);
        verticalLayoutCarte->setObjectName("verticalLayoutCarte");
        verticalLayoutCarte->setContentsMargins(0, 0, 0, 0);
        labelCarte = new QLabel(layoutWidget3);
        labelCarte->setObjectName("labelCarte");
        labelCarte->setFont(font1);
        labelCarte->setAlignment(Qt::AlignCenter);

        verticalLayoutCarte->addWidget(labelCarte);

        labelCarteNumerique = new QLabel(layoutWidget3);
        labelCarteNumerique->setObjectName("labelCarteNumerique");
        labelCarteNumerique->setFont(font2);
        labelCarteNumerique->setAlignment(Qt::AlignCenter);

        verticalLayoutCarte->addWidget(labelCarteNumerique);

        pushButtonPret = new QPushButton(ClientBJ);
        pushButtonPret->setObjectName("pushButtonPret");
        pushButtonPret->setGeometry(QRect(570, 550, 181, 41));
        pushButtonPret->setFont(font);
        pushButtonMiser = new QPushButton(ClientBJ);
        pushButtonMiser->setObjectName("pushButtonMiser");
        pushButtonMiser->setGeometry(QRect(570, 400, 181, 41));
        pushButtonMiser->setFont(font);
        layoutWidget_2 = new QWidget(ClientBJ);
        layoutWidget_2->setObjectName("layoutWidget_2");
        layoutWidget_2->setGeometry(QRect(340, 410, 111, 81));
        verticalLayoutCagnotte = new QVBoxLayout(layoutWidget_2);
        verticalLayoutCagnotte->setObjectName("verticalLayoutCagnotte");
        verticalLayoutCagnotte->setContentsMargins(0, 0, 0, 0);
        labelCagnotte = new QLabel(layoutWidget_2);
        labelCagnotte->setObjectName("labelCagnotte");
        labelCagnotte->setFont(font1);
        labelCagnotte->setAlignment(Qt::AlignCenter);

        verticalLayoutCagnotte->addWidget(labelCagnotte);

        labelMontantCagnotte = new QLabel(layoutWidget_2);
        labelMontantCagnotte->setObjectName("labelMontantCagnotte");
        labelMontantCagnotte->setFont(font2);
        labelMontantCagnotte->setAlignment(Qt::AlignCenter);

        verticalLayoutCagnotte->addWidget(labelMontantCagnotte);

        lineEditMdp = new QLineEdit(ClientBJ);
        lineEditMdp->setObjectName("lineEditMdp");
        lineEditMdp->setGeometry(QRect(20, 480, 181, 31));
        lineEditMdp->setFont(font1);
        lineEditMdp->setMaxLength(15);
        lineEditMdp->setEchoMode(QLineEdit::Password);

        retranslateUi(ClientBJ);
        QObject::connect(pushButtonQuitter, &QPushButton::clicked, ClientBJ, qOverload<>(&QWidget::close));

        QMetaObject::connectSlotsByName(ClientBJ);
    } // setupUi

    void retranslateUi(QWidget *ClientBJ)
    {
        ClientBJ->setWindowTitle(QCoreApplication::translate("ClientBJ", "ClientBJ", nullptr));
        pushButtonPiocher->setText(QCoreApplication::translate("ClientBJ", "Piocher", nullptr));
        pushButtonRegles->setText(QCoreApplication::translate("ClientBJ", "R\303\250gles", nullptr));
        pushButtonRester->setText(QCoreApplication::translate("ClientBJ", "Rester", nullptr));
        labelMontantMise->setText(QCoreApplication::translate("ClientBJ", "Mise :", nullptr));
        labelMontantMiseNumerique->setText(QCoreApplication::translate("ClientBJ", "0", nullptr));
        pushButtonConnexion->setText(QCoreApplication::translate("ClientBJ", "Connexion", nullptr));
        pushButtonQuitter->setText(QCoreApplication::translate("ClientBJ", "Quitter", nullptr));
        lineEditPseudo->setInputMask(QString());
        lineEditPseudo->setText(QString());
        lineEditPseudo->setPlaceholderText(QCoreApplication::translate("ClientBJ", "Pseudo", nullptr));
        labelMise->setText(QCoreApplication::translate("ClientBJ", "Mise :", nullptr));
        lineEditMise->setPlaceholderText(QCoreApplication::translate("ClientBJ", "Mise >= 100", nullptr));
        lineEditAdresse->setText(QCoreApplication::translate("ClientBJ", "172.18.58.98", nullptr));
        labelCarte->setText(QCoreApplication::translate("ClientBJ", "Total des cartes :", nullptr));
        labelCarteNumerique->setText(QCoreApplication::translate("ClientBJ", "0", nullptr));
        pushButtonPret->setText(QCoreApplication::translate("ClientBJ", "Rejoindre", nullptr));
        pushButtonMiser->setText(QCoreApplication::translate("ClientBJ", "Miser", nullptr));
        labelCagnotte->setText(QCoreApplication::translate("ClientBJ", "Cagnotte :", nullptr));
        labelMontantCagnotte->setText(QCoreApplication::translate("ClientBJ", "0", nullptr));
        lineEditMdp->setInputMask(QString());
        lineEditMdp->setText(QString());
        lineEditMdp->setPlaceholderText(QCoreApplication::translate("ClientBJ", "Mot de passe", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ClientBJ: public Ui_ClientBJ {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CLIENTBJ_H
