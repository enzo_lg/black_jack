#include "joueur.h"

Joueur::Joueur():
    argent(1000),
    pret(false),
    coucher(false),
    mise(0),
    tour(0)
{

}

const QString &Joueur::getNom() const
{
    return nom;
}

void Joueur::setNom(const QString &newNom)
{
    nom = newNom;
}

int Joueur::getArgent() const
{
    return argent;
}

void Joueur::setArgent(int newArgent)
{
    argent += newArgent;
}

const QList<Carte> &Joueur::getListCarte() const
{
    return listCarte;
}

void Joueur::ajouterCarte(Carte carte)
{
    listCarte.append(carte);
}

int Joueur::getScore() const
{
    return score;
}

void Joueur::setScore(int newScore)
{
    score = newScore;
}

QTcpSocket *Joueur::getSockJoueur() const
{
    return joueur;
}

void Joueur::setSockJoueur(QTcpSocket *newJoueur)
{
    joueur = newJoueur;
}

bool Joueur::getPret() const
{
    return pret;
}

void Joueur::setPret(bool newPret)
{
    pret = newPret;
}

bool Joueur::getCoucher() const
{
    return coucher;
}

void Joueur::setCoucher(bool newCoucher)
{
    coucher = newCoucher;
}

int Joueur::getMise() const
{
    return mise;
}

void Joueur::setMise(int newMise)
{
    mise = newMise;
}

int Joueur::getTour() const
{
    return tour;
}

void Joueur::setTour(int newTour)
{
    tour += newTour;
}
