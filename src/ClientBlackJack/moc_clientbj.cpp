/****************************************************************************
** Meta object code from reading C++ file 'clientbj.h'
**
** Created by: The Qt Meta Object Compiler version 68 (Qt 6.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "clientbj.h"
#include <QtGui/qtextcursor.h>
#include <QtCore/qmetatype.h>

#if __has_include(<QtCore/qtmochelpers.h>)
#include <QtCore/qtmochelpers.h>
#else
QT_BEGIN_MOC_NAMESPACE
#endif


#include <memory>

#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'clientbj.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 68
#error "This file was generated using the moc from 6.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

#ifndef Q_CONSTINIT
#define Q_CONSTINIT
#endif

QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
QT_WARNING_DISABLE_GCC("-Wuseless-cast")
namespace {

#ifdef QT_MOC_HAS_STRINGDATA
struct qt_meta_stringdata_CLASSClientBJENDCLASS_t {};
static constexpr auto qt_meta_stringdata_CLASSClientBJENDCLASS = QtMocHelpers::stringData(
    "ClientBJ",
    "EnvoyerCommande",
    "",
    "commande",
    "onQTcpSocket_connected",
    "onQTcpSocket_disconnected",
    "onQTcpSocket_readyRead",
    "onQTcpSocket_errorOccured",
    "QAbstractSocket::SocketError",
    "socketError",
    "on_pushButtonConnexion_clicked",
    "on_lineEditPseudo_textChanged",
    "arg1",
    "on_pushButtonPiocher_clicked",
    "on_pushButtonRester_clicked",
    "on_pushButtonMiser_clicked",
    "on_pushButtonPret_clicked",
    "on_lineEditMdp_textChanged",
    "on_pushButtonRegles_clicked"
);
#else  // !QT_MOC_HAS_STRING_DATA
struct qt_meta_stringdata_CLASSClientBJENDCLASS_t {
    uint offsetsAndSizes[38];
    char stringdata0[9];
    char stringdata1[16];
    char stringdata2[1];
    char stringdata3[9];
    char stringdata4[23];
    char stringdata5[26];
    char stringdata6[23];
    char stringdata7[26];
    char stringdata8[29];
    char stringdata9[12];
    char stringdata10[31];
    char stringdata11[30];
    char stringdata12[5];
    char stringdata13[29];
    char stringdata14[28];
    char stringdata15[27];
    char stringdata16[26];
    char stringdata17[27];
    char stringdata18[28];
};
#define QT_MOC_LITERAL(ofs, len) \
    uint(sizeof(qt_meta_stringdata_CLASSClientBJENDCLASS_t::offsetsAndSizes) + ofs), len 
Q_CONSTINIT static const qt_meta_stringdata_CLASSClientBJENDCLASS_t qt_meta_stringdata_CLASSClientBJENDCLASS = {
    {
        QT_MOC_LITERAL(0, 8),  // "ClientBJ"
        QT_MOC_LITERAL(9, 15),  // "EnvoyerCommande"
        QT_MOC_LITERAL(25, 0),  // ""
        QT_MOC_LITERAL(26, 8),  // "commande"
        QT_MOC_LITERAL(35, 22),  // "onQTcpSocket_connected"
        QT_MOC_LITERAL(58, 25),  // "onQTcpSocket_disconnected"
        QT_MOC_LITERAL(84, 22),  // "onQTcpSocket_readyRead"
        QT_MOC_LITERAL(107, 25),  // "onQTcpSocket_errorOccured"
        QT_MOC_LITERAL(133, 28),  // "QAbstractSocket::SocketError"
        QT_MOC_LITERAL(162, 11),  // "socketError"
        QT_MOC_LITERAL(174, 30),  // "on_pushButtonConnexion_clicked"
        QT_MOC_LITERAL(205, 29),  // "on_lineEditPseudo_textChanged"
        QT_MOC_LITERAL(235, 4),  // "arg1"
        QT_MOC_LITERAL(240, 28),  // "on_pushButtonPiocher_clicked"
        QT_MOC_LITERAL(269, 27),  // "on_pushButtonRester_clicked"
        QT_MOC_LITERAL(297, 26),  // "on_pushButtonMiser_clicked"
        QT_MOC_LITERAL(324, 25),  // "on_pushButtonPret_clicked"
        QT_MOC_LITERAL(350, 26),  // "on_lineEditMdp_textChanged"
        QT_MOC_LITERAL(377, 27)   // "on_pushButtonRegles_clicked"
    },
    "ClientBJ",
    "EnvoyerCommande",
    "",
    "commande",
    "onQTcpSocket_connected",
    "onQTcpSocket_disconnected",
    "onQTcpSocket_readyRead",
    "onQTcpSocket_errorOccured",
    "QAbstractSocket::SocketError",
    "socketError",
    "on_pushButtonConnexion_clicked",
    "on_lineEditPseudo_textChanged",
    "arg1",
    "on_pushButtonPiocher_clicked",
    "on_pushButtonRester_clicked",
    "on_pushButtonMiser_clicked",
    "on_pushButtonPret_clicked",
    "on_lineEditMdp_textChanged",
    "on_pushButtonRegles_clicked"
};
#undef QT_MOC_LITERAL
#endif // !QT_MOC_HAS_STRING_DATA
} // unnamed namespace

Q_CONSTINIT static const uint qt_meta_data_CLASSClientBJENDCLASS[] = {

 // content:
      11,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags, initial metatype offsets
       1,    1,   92,    2, 0x08,    1 /* Private */,
       4,    0,   95,    2, 0x08,    3 /* Private */,
       5,    0,   96,    2, 0x08,    4 /* Private */,
       6,    0,   97,    2, 0x08,    5 /* Private */,
       7,    1,   98,    2, 0x08,    6 /* Private */,
      10,    0,  101,    2, 0x08,    8 /* Private */,
      11,    1,  102,    2, 0x08,    9 /* Private */,
      13,    0,  105,    2, 0x08,   11 /* Private */,
      14,    0,  106,    2, 0x08,   12 /* Private */,
      15,    0,  107,    2, 0x08,   13 /* Private */,
      16,    0,  108,    2, 0x08,   14 /* Private */,
      17,    1,  109,    2, 0x08,   15 /* Private */,
      18,    0,  112,    2, 0x08,   17 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QChar,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   12,
    QMetaType::Void,

       0        // eod
};

Q_CONSTINIT const QMetaObject ClientBJ::staticMetaObject = { {
    QMetaObject::SuperData::link<QWidget::staticMetaObject>(),
    qt_meta_stringdata_CLASSClientBJENDCLASS.offsetsAndSizes,
    qt_meta_data_CLASSClientBJENDCLASS,
    qt_static_metacall,
    nullptr,
    qt_incomplete_metaTypeArray<qt_meta_stringdata_CLASSClientBJENDCLASS_t,
        // Q_OBJECT / Q_GADGET
        QtPrivate::TypeAndForceComplete<ClientBJ, std::true_type>,
        // method 'EnvoyerCommande'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        QtPrivate::TypeAndForceComplete<QChar, std::false_type>,
        // method 'onQTcpSocket_connected'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'onQTcpSocket_disconnected'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'onQTcpSocket_readyRead'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'onQTcpSocket_errorOccured'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        QtPrivate::TypeAndForceComplete<QAbstractSocket::SocketError, std::false_type>,
        // method 'on_pushButtonConnexion_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_lineEditPseudo_textChanged'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        QtPrivate::TypeAndForceComplete<const QString &, std::false_type>,
        // method 'on_pushButtonPiocher_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_pushButtonRester_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_pushButtonMiser_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_pushButtonPret_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_lineEditMdp_textChanged'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        QtPrivate::TypeAndForceComplete<const QString &, std::false_type>,
        // method 'on_pushButtonRegles_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>
    >,
    nullptr
} };

void ClientBJ::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ClientBJ *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->EnvoyerCommande((*reinterpret_cast< std::add_pointer_t<QChar>>(_a[1]))); break;
        case 1: _t->onQTcpSocket_connected(); break;
        case 2: _t->onQTcpSocket_disconnected(); break;
        case 3: _t->onQTcpSocket_readyRead(); break;
        case 4: _t->onQTcpSocket_errorOccured((*reinterpret_cast< std::add_pointer_t<QAbstractSocket::SocketError>>(_a[1]))); break;
        case 5: _t->on_pushButtonConnexion_clicked(); break;
        case 6: _t->on_lineEditPseudo_textChanged((*reinterpret_cast< std::add_pointer_t<QString>>(_a[1]))); break;
        case 7: _t->on_pushButtonPiocher_clicked(); break;
        case 8: _t->on_pushButtonRester_clicked(); break;
        case 9: _t->on_pushButtonMiser_clicked(); break;
        case 10: _t->on_pushButtonPret_clicked(); break;
        case 11: _t->on_lineEditMdp_textChanged((*reinterpret_cast< std::add_pointer_t<QString>>(_a[1]))); break;
        case 12: _t->on_pushButtonRegles_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType(); break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType(); break;
            case 0:
                *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType::fromType< QAbstractSocket::SocketError >(); break;
            }
            break;
        }
    }
}

const QMetaObject *ClientBJ::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ClientBJ::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CLASSClientBJENDCLASS.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int ClientBJ::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    }
    return _id;
}
QT_WARNING_POP
