#ifndef CARTE_H
#define CARTE_H


#include <QObject>
#include <QGraphicsScene>
#include <QGraphicsItem>

class Carte
{

public:
    explicit Carte(QString _symbole,int _valeur, bool _recto);
    Carte();
    
    const QString &getSymbole() const;
    void setSymbole(const QString &newSymbole);
    
    int getValeur() const;
    void setValeur(int newValeur);
    

    const QString &getCheminImg() const;
    void setCheminImg(const QString &newCheminImg);

    friend QDataStream& operator>>(QDataStream& in, Carte& v) {
            in >> v.symbole >> v.valeur >> v.recto >> v.cheminImg;
            return in;
        }
    friend QDataStream& operator<<(QDataStream& out, const Carte& v) {
           out << v.symbole << v.valeur << v.recto << v.cheminImg;
           return out;
       }

private:
    QString symbole;
    int valeur;
    QString cheminImg;

    bool recto;


};

#endif // CARTE_H

