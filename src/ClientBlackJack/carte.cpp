#include "carte.h"

Carte::Carte(QString _symbole, int _valeur, bool _recto)
    : symbole(_symbole),
      valeur(_valeur),recto(_recto)

{
    if(recto==true){
        cheminImg=":/img/recto.png";
    }
    else{
        cheminImg=":/img/"+QString::number(valeur)+symbole+".png";
    }
}

Carte::Carte()
{

}

const QString &Carte::getSymbole() const
{
    return symbole;
}

void Carte::setSymbole(const QString &newSymbole)
{
    symbole = newSymbole;
}

int Carte::getValeur() const
{
    return valeur;
}

void Carte::setValeur(int newValeur)
{
    valeur = newValeur;
}

const QString &Carte::getCheminImg() const
{
    return cheminImg;
}
