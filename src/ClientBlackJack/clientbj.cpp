#include "clientbj.h"
#include "ui_clientbj.h"

ClientBJ::ClientBJ(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::ClientBJ)
{
    ui->setupUi(this);

    ui->pushButtonConnexion->setEnabled(false);
    ui->pushButtonPiocher->setEnabled(false);
    ui->pushButtonRester->setEnabled(false);
    ui->pushButtonPret->setEnabled(false);
    ui->pushButtonMiser->setEnabled(false);
    ui->lineEditPseudo->setEnabled(true);
    ui->lineEditMdp->setEnabled(true);



    ui->lineEditPseudo->setValidator(new QRegularExpressionValidator(QRegularExpression("[A-Za-z0-9_]{0,255}"),this));
    ui->lineEditMdp->setValidator(new QRegularExpressionValidator(QRegularExpression("[A-Za-z0-9@#$!_]{0,255}"),this));
    ui->lineEditMise->setValidator(new QRegularExpressionValidator(QRegularExpression("[0-9]{0,10}"),this));

    // connection signal/slot pour la socket
    connect(&socketClient,&QTcpSocket::connected,this,&ClientBJ::onQTcpSocket_connected);
    connect(&socketClient,&QTcpSocket::disconnected,this,&ClientBJ::onQTcpSocket_disconnected);
    connect(&socketClient,&QTcpSocket::readyRead,this,&ClientBJ::onQTcpSocket_readyRead);
    connect(&socketClient,&QTcpSocket::errorOccurred,this,&ClientBJ::onQTcpSocket_errorOccured);

    maScene.setSceneRect(0, 0, 541, 381);
    QGraphicsRectItem *tapis = new QGraphicsRectItem(0,0,541,381);
    QPixmap texture(":/img/tapisblackjack.png");
    QPixmap resizedPixmap = texture.scaled(0.715 * texture.width(), 0.95 * texture.height());
    tapis->setBrush(resizedPixmap);
    maScene.addItem(tapis);
    ui->maVue->rotate(180);
    ui->maVue->fitInView(maScene.sceneRect(),Qt::KeepAspectRatio);
    ui->maVue->setScene(&maScene);


    maScenedeux.setSceneRect(0, 0, 179, 309);
    QGraphicsRectItem *fondCartes = new QGraphicsRectItem(0,0,179,310);
    QPixmap textures(":/img/fondpourcartes.jpg");
    QPixmap resizedPixmaps = textures.scaled(0.25 * textures.width(), 0.25 * textures.height());
    fondCartes->setBrush(resizedPixmaps);
    maScenedeux.addItem(fondCartes);
    ui->maMain->fitInView(maScenedeux.sceneRect(),Qt::KeepAspectRatio);
    ui->maMain->setScene(&maScenedeux);


}

ClientBJ::~ClientBJ()
{
    delete ui;
}


void ClientBJ::onQTcpSocket_connected()
{
    qDebug() << "Connexion au serveur";
    ui->pushButtonConnexion->setText("Déconnexion");
    ui->lineEditMise->setEnabled(true);
    ui->pushButtonMiser->setEnabled(true);
    ui->lineEditPseudo->setEnabled(false);
    ui->lineEditMdp->setEnabled(false);
    quint16 taille=0;
    QBuffer tampon;
    QChar commande = 'N';
    QString pseudo = ui->lineEditPseudo->text();
    QString mdp = ui->lineEditMdp->text();
    tampon.open(QIODevice::WriteOnly);
    // association du tampon au flux de sortie
    QDataStream out(&tampon);
    // construction de la trame
    qDebug() << "aa"<<  pseudo << "" << mdp;
    out<<taille<<commande<<pseudo<<mdp;
    // calcul de la taille de la trame
    taille=(static_cast<quint16>(tampon.size()))-sizeof(taille);
    // placement sur la premiere position du flux pour pouvoir modifier la taille
    tampon.seek(0);
    //modification de la trame avec la taille reel de la trame
    out<<taille;
    // envoi du QByteArray du tampon via la socket
    socketClient.write(tampon.buffer());

    ui->labelMontantMiseNumerique->setText("0");
    ui->labelCarteNumerique->setText("0");

    player.setAudioOutput(&sortieAudio);
    player.setSource(QUrl::fromLocalFile("bonjour_voix.mp3"));
    sortieAudio.setVolume(50);
    player.play();
}

void ClientBJ::onQTcpSocket_disconnected()
{
    qDebug() << "Déconnexion du serveur";
    ui->pushButtonConnexion->setText("Connexion");
    ui->lineEditMise->setEnabled(false);
    ui->pushButtonPiocher->setEnabled(false);
    ui->pushButtonRester->setEnabled(false);
    ui->pushButtonPret->setEnabled(false);
    ui->pushButtonMiser->setEnabled(false);
    ui->lineEditPseudo->setEnabled(true);
    ui->lineEditMdp->setEnabled(true);

    ui->pushButtonPret->setText("Rejoindre");

    QList<QGraphicsItem*> items = maScenedeux.items();
    for (QGraphicsItem *item : items) {
        // Vérifier si l'objet est de type QGraphicsRectItem (carte)
        if (item->type() == QGraphicsRectItem::Type) {
            maScenedeux.removeItem(item);
            delete item;
        }
    }
    maScenedeux.setSceneRect(0, 0, 179, 309);
    QGraphicsRectItem *fondCartes = new QGraphicsRectItem(0,0,200,350);
    QPixmap textures(":/img/fondpourcartes.jpg");
    QPixmap resizedPixmaps = textures.scaled(0.25 * textures.width(), 0.25 * textures.height());
    fondCartes->setBrush(resizedPixmaps);
    maScenedeux.addItem(fondCartes);
    ui->maMain->fitInView(maScenedeux.sceneRect(),Qt::KeepAspectRatio);
    ui->maMain->setScene(&maScenedeux);

    player.setAudioOutput(&sortieAudio);
    player.setSource(QUrl::fromLocalFile("aurevoir_voix.mp3"));
    sortieAudio.setVolume(50);
    player.play();
}
void ClientBJ::onQTcpSocket_readyRead()
{
    quint16 taille=0;
    QList <Carte> mesCartes;
    QChar commande;
    QString pseudoVainqueur;
    int miseVerify;
    int mise;
    int cagnotte;
    int scoreCartes;
    int nombreCartes;

    if (socketClient.bytesAvailable() >= (qint64)sizeof(taille))
    {
        QDataStream  in(&socketClient);
        in >> taille;
        qDebug() << taille;
        if (socketClient.bytesAvailable() >= (qint64)taille)
        {
            in >> commande;
            qDebug() << "commande : " << commande;
            //Tirer
            if(commande == 'T'){
                in >> mesCartes >> scoreCartes >> nombreCartes;
                ui->labelCarteNumerique->setText(QString::number(scoreCartes));
                QString chemin;
                qDebug() << "taille paquet : " << mesCartes.size() << "numero" << mesCartes.at(mesCartes.size()-1).getValeur()<<"symbole " << mesCartes.at(mesCartes.size()-1).getSymbole() << "score des cartes : " << scoreCartes << "Nombre de carte(s) sur le tapis : " << nombreCartes;
                chemin = mesCartes.at(mesCartes.size()-1).getCheminImg();
                QGraphicsRectItem *tapis = new QGraphicsRectItem(0,0,95,130);
                qDebug() << "Nombre de carte sur le tapis : " << nombreCartes;
                tapis->setPos(42,mesCartes.size()*30-20);
                qDebug() << "Chemin de la carte : " << chemin;
                QPixmap texture(chemin);
                QPixmap resizedPixmap = texture.scaled(1 * texture.width(), 1 * texture.height());
                tapis->setBrush(resizedPixmap);
                maScenedeux.addItem(tapis);
                ui->pushButtonPiocher->setEnabled(false);
                QEventLoop timer;
                QTimer::singleShot(500,&timer,&QEventLoop::quit);
                timer.exec();
                ui->pushButtonPiocher->setEnabled(true);
            }
            //Miser
            if(commande == 'M'){
                in >> miseVerify>>mise>>cagnotte;
                qDebug() << "Mise 1 ou 0 : " << miseVerify;
                if(miseVerify == 1){
                    ui->lineEditMise->setPlaceholderText("Montant inférieur à 100");

                    player.setAudioOutput(&sortieAudio);
                    player.setSource(QUrl::fromLocalFile("mise_mini_voix.mp3"));
                    sortieAudio.setVolume(50);
                    player.play();
                }
                if(miseVerify == 0){
                    qDebug() << "Mise montant : " << mise;
                    ui->labelMontantMiseNumerique->setText(QString::number(mise));
                    ui->labelMontantCagnotte->setText(QString::number(cagnotte));
                    ui->pushButtonMiser->setEnabled(false);
                    ui->pushButtonPret->setEnabled(true);

                    player.setAudioOutput(&sortieAudio);
                    player.setSource(QUrl::fromLocalFile("mise.mp3"));
                    sortieAudio.setVolume(50);
                    player.play();
                }
            }
            //Cagnotte
            if(commande == 'C'){
                in >> cagnotte;
                qDebug() << "Cagnotte : " << cagnotte;
                ui->labelMontantCagnotte->setText(QString::number(cagnotte));
            }
            //Victoire
            if(commande == 'V'){
                in >> pseudoVainqueur;
                QString messageVictoire = pseudoVainqueur +" a gagner la partie !";
                QMessageBox::about(this,"VAINQUEUR",messageVictoire);
                //ui->pushButtonPret->setText("Rejoindre");
                ui->pushButtonMiser->setEnabled(false);
                ui->pushButtonPiocher->setEnabled(false);
                ui->pushButtonRester->setEnabled(false);

                player.setAudioOutput(&sortieAudio);
                player.setSource(QUrl::fromLocalFile("gain_dargent.mp3"));
                sortieAudio.setVolume(50);
                player.play();
                QEventLoop timer;
                QTimer::singleShot(1000,&timer,&QEventLoop::quit);
                timer.exec();
                player.setAudioOutput(&sortieAudio);
                player.setSource(QUrl::fromLocalFile("joueur1_gagne_voix.mp3"));
                sortieAudio.setVolume(50);
                player.play();

            }
            //Défaite
            if(commande == 'D'){
                QMessageBox::about(this,"DEFAITE","Vous avez perdu la partie...");
                ui->pushButtonMiser->setEnabled(false);
                ui->pushButtonPiocher->setEnabled(false);
                ui->pushButtonRester->setEnabled(false);

                player.setAudioOutput(&sortieAudio);
                player.setSource(QUrl::fromLocalFile("perdu_voix.mp3"));
                sortieAudio.setVolume(50);
                player.play();
            }
            //Lancer partie quand tout le monde est prêt
            if(commande == 'L'){
                ui->pushButtonPret->setText("En ligne");
                ui->pushButtonPiocher->setEnabled(true);
                ui->pushButtonRester->setEnabled(true);
                player.setAudioOutput(&sortieAudio);
                player.setSource(QUrl::fromLocalFile("croupier_gagne.mp3"));
                sortieAudio.setVolume(50);
                player.play();
            }
            //Rester
            if(commande == 'R'){
                ui->pushButtonMiser->setEnabled(false);
                ui->pushButtonPiocher->setEnabled(false);
                ui->pushButtonRester->setEnabled(false);
                ui->pushButtonPret->setEnabled(false);
            }
        }
    }
}

void ClientBJ::onQTcpSocket_errorOccured(QAbstractSocket::SocketError socketError)
{
    qDebug() << socketError;
}

void ClientBJ::on_pushButtonConnexion_clicked()
{
    if(ui->pushButtonConnexion->text()=="Connexion"){
        socketClient.connectToHost( ui->lineEditAdresse->text(), ui->spinBoxPort->value());
    }
    if(ui->pushButtonConnexion->text()=="Déconnexion"){
        socketClient.disconnectFromHost();
    }
}

void ClientBJ::on_lineEditPseudo_textChanged(const QString &arg1)
{
    QString mdp = ui->lineEditMdp->text();
    if(arg1.length() <= 2){
        ui->pushButtonConnexion->setEnabled(false);
    }
    if(arg1.length() > 2){
        if(mdp.length() > 3){
            ui->pushButtonConnexion->setEnabled(true);
        }
        if(mdp.length() <= 3){
            ui->pushButtonConnexion->setEnabled(false);
        }
    }
}

void ClientBJ::on_lineEditMdp_textChanged(const QString &arg1)
{
    QString pseudo= ui->lineEditPseudo->text();
    if(arg1.length() <= 3){
        ui->pushButtonConnexion->setEnabled(false);
    }
    if(arg1.length() > 3){
        if(pseudo.length() > 2){
            ui->pushButtonConnexion->setEnabled(true);
        }
        if(pseudo.length() <= 2){
            ui->pushButtonConnexion->setEnabled(false);
        }
    }
}

void ClientBJ::EnvoyerCommande(QChar commande)
{
    quint16 taille=0;
    QBuffer tampon;
    tampon.open(QIODevice::WriteOnly);
    // association du tampon au flux de sortie
    QDataStream out(&tampon);
    // construction de la trame
    out<<taille<<commande;
    // calcul de la taille de la trame
    taille=(static_cast<quint16>(tampon.size()))-sizeof(taille);
    // placement sur la premiere position du flux pour pouvoir modifier la taille
    tampon.seek(0);
    //modification de la trame avec la taille reel de la trame
    out<<taille;
    // envoi du QByteArray du tampon via la socket
    socketClient.write(tampon.buffer());
}

void ClientBJ::on_pushButtonPiocher_clicked()
{
    EnvoyerCommande('T');
    player.setAudioOutput(&sortieAudio);
    player.setSource(QUrl::fromLocalFile("bruit_carte.mp3"));
    sortieAudio.setVolume(50);
    player.play();
}

void ClientBJ::on_pushButtonRester_clicked()
{
    EnvoyerCommande('R');
    ui->pushButtonMiser->setEnabled(false);
    ui->pushButtonPiocher->setEnabled(false);
    ui->pushButtonRester->setEnabled(false);
}

void ClientBJ::on_pushButtonMiser_clicked()
{
    quint16 taille=0;
    QBuffer tampon;
    QChar commande = 'M';
    int mise = ui->lineEditMise->text().toInt();
    qDebug() << "votre mise" << mise;
    ui->lineEditMise->clear();
    tampon.open(QIODevice::WriteOnly);
    // association du tampon au flux de sortie
    QDataStream out(&tampon);
    // construction de la trame
    out<<taille<<commande<<mise;
    // calcul de la taille de la trame
    taille=(static_cast<quint16>(tampon.size()))-sizeof(taille);
    // placement sur la premiere position du flux pour pouvoir modifier la taille
    tampon.seek(0);
    //modification de la trame avec la taille reel de la trame
    out<<taille;
    // envoi du QByteArray du tampon via la socket
    socketClient.write(tampon.buffer());
}

void ClientBJ::on_pushButtonPret_clicked()
{
    EnvoyerCommande('P');
    ui->pushButtonPret->setText("Prêt");
    ui->pushButtonPret->setEnabled(false);
}

void ClientBJ::on_pushButtonRegles_clicked()
{
    const char* commande = "xdg-open";
    const char* cheminFichierHTML = "index.html";

    // Construction de la commande
    std::string commandeComplete = std::string(commande) + " " + cheminFichierHTML;

    // Exécution de la commande système
    int resultat = std::system(commandeComplete.c_str());
}
