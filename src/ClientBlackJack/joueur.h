#ifndef JOUEUR_H
#define JOUEUR_H

#include "carte.h"
#include <QTcpSocket>


class Joueur
{
public:
    Joueur();
    const QString &getNom() const;
    void setNom(const QString &newNom);

    int getArgent() const;
    void setArgent(int newArgent);

    const QList<Carte> &getListCarte() const;
    void ajouterCarte(const Carte carte);

    int getScore() const;
    void setScore(int newScore);

    QTcpSocket *getSockJoueur() const;
    void setSockJoueur(QTcpSocket *newJoueur);

    bool getPret() const;
    void setPret(bool newPret);

    bool getCoucher() const;
    void setCoucher(bool newCoucher);

    int getMise() const;
    void setMise(int newMise);

    int getTour() const;
    void setTour(int newTour);

private:
    QString nom;
    int argent;
    QList<Carte> listCarte;
    int score;
    QTcpSocket *joueur;
    bool pret;
    bool coucher;
    int mise;
    int tour;
};

#endif // JOUEUR_H

