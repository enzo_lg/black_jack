#ifndef CLIENTBJ_H
#define CLIENTBJ_H

#include <QWidget>
#include <QToolButton>
#include <QGridLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QTimer>
#include <QAbstractSocket>
#include <QTcpSocket>
#include <QBuffer>
#include <QRegularExpressionValidator>
#include <QMessageBox>
#include <QProcess>
#include <QGraphicsScene>
#include <QGraphicsPolygonItem>
#include <QEventLoop>
#include "carte.h"
#include "joueur.h"
#include <QMediaPlayer>
#include <QAudioOutput>

QT_BEGIN_NAMESPACE
namespace Ui { class ClientBJ; }
QT_END_NAMESPACE

class ClientBJ : public QWidget
{
    Q_OBJECT

public:
    ClientBJ(QWidget *parent = nullptr);
    ~ClientBJ();


private slots:
    void EnvoyerCommande(QChar commande);

    void onQTcpSocket_connected();
    void onQTcpSocket_disconnected();
    void onQTcpSocket_readyRead();
    void onQTcpSocket_errorOccured(QAbstractSocket::SocketError socketError);

    void on_pushButtonConnexion_clicked();
    void on_lineEditPseudo_textChanged(const QString &arg1);
    void on_pushButtonPiocher_clicked();
    void on_pushButtonRester_clicked();
    void on_pushButtonMiser_clicked();
    void on_pushButtonPret_clicked();

    void on_lineEditMdp_textChanged(const QString &arg1);

    void on_pushButtonRegles_clicked();

private:
    Ui::ClientBJ *ui;
    QTcpSocket socketClient;
    QString typeDeDemande;
    QGraphicsScene maScene;
    QGraphicsScene maScenedeux;
    QList <Carte> cartes;
    QChar lettre;
    QString nomFichier;
    QMediaPlayer player;
    QAudioOutput sortieAudio;

};
#endif // CLIENTBJ_H
